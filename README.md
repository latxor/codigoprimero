### Ejemplos Code First - Código Primero ###

**CodigoPrimero**: Ejemplo donde se muestra como utilizar la funcion include para traer datos de una clase a partir de otra, por ejemplo: traer información de los hijos a partir del padre y viceversa.

**Login**: Maqueta de un sistema de logueo utilizando código primero, Interfaces, Repositorios y Singleton.

**Autor: Carlos Gaviria Gallego**