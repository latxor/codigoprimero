﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login.Datos
{
    public interface IRepositorio<T>
    {
        T ObTenerPorId(int id);
        T ObTenerPorNombre(string nombre);
        List<T> ObtenerTodos();
        void Guardar(T entidad);      
        void Eliminar(T entidad);
    }
}
