﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login.Datos
{
    public interface IRol
    {
         int Id { get; set; }
         string Nombre { get; set; }
    }
}
