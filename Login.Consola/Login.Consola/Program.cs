﻿using Login.Infrastructura;
using Login.Infrastructura.Contexto;
using Login.Infrastructura.Repositorio;
using Login.Infrastructura.Singlenton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login.Consola
{
    class Program
    {
        static void Main(string[] args)
        {
            RepositorioUsuario _usuarioRepositorio = new RepositorioUsuario();
            RepositorioRol _rolRepositorio = new RepositorioRol();
            Contexto contexto = new Contexto();
            contexto.Database.CreateIfNotExists();

            #region Operaciones con roles
            //Crear Roles Primero

            _rolRepositorio.Guardar(new Infrastructura.Rol() { Nombre = "Administrador" });
            _rolRepositorio.Guardar(new Infrastructura.Rol() { Nombre = "Normal" });

            //Buscar Rol Por Id
            var rol = _rolRepositorio.ObTenerPorId(1);

            //Buscar Rol Por nombre
            var rolnombre = _rolRepositorio.ObTenerPorNombre("Administrador");

            //Buscar Todos Los Roles
            var todosroles = _rolRepositorio.ObtenerTodos();
            #endregion

            #region Operaciones Usuario
            
            //Crear Usuario
            _usuarioRepositorio.Guardar(new Infrastructura.Usuario() { NombreUsuario = "Administrador", Password = "123456" });
            _usuarioRepositorio.Guardar(new Infrastructura.Usuario() { NombreUsuario = "Auxiliar", Password = "123456" });

            //Buscar Usuario Por Id
            var usuarioPorId =_usuarioRepositorio.ObTenerPorId(1);

            //Buscar Usuario Por Nombre
            var usuarioPorNombre = _usuarioRepositorio.ObTenerPorNombre("Auxiliar");

            //Buscar Usuario Por Id
            var todosusuarios = _usuarioRepositorio.ObtenerTodos();

            //Agregar Roles a nuevo usuario
            Usuario usuario = new Usuario() { NombreUsuario = "AuxContable", Password = "124567" };
            _usuarioRepositorio.Guardar(usuario);//Una vez guardado en base de datos entity asigna el Id al usuario


            List<Rol> RolesParaELNuevoUsuario = new List<Rol>();
            RolesParaELNuevoUsuario = _rolRepositorio.ObtenerTodos();

            _usuarioRepositorio.AgregarRolesAUsuario(usuario.Id, RolesParaELNuevoUsuario);


            //Agregar Roles a usuario ya creados

            //1.Buscas al usuario
            var usuarioviejo = _usuarioRepositorio.ObTenerPorNombre("Auxiliar");

            //2.Buscas los roles, en este ejemplo busco todos los roles
            var roles = _rolRepositorio.ObtenerTodos();

            //3. se utiliza el metodo AgregarRolesAUsuario del repositorio de usuario 
            _usuarioRepositorio.AgregarRolesAUsuario(usuarioviejo.Id, roles);


            //Mirar si el usuario tiene determinado rol o no, si lo tiene regresa True sino False
            var tienerol = _usuarioRepositorio.EstaEnRol("Auxiliar", "Administrador");

            #endregion

            #region Ejemplo de como tener permanentemente la información de usuario logueado

            //Para poder tener siempre disponible los datos del usuario logueado se debe utilizar algo que se llama singleton(lee mas sobre esto), que no es mas una clase estatica
            //que la vas a poder utilizar en todos lados y a la cual le vas a poder sacar la información sobre el usuario logueado.

            //1.Supon que el usuario inserto nombre de usuario y contraseña y le dio click al boton loguearse, lo primero que hay que hacer es utilizar el repositorio de usuario para buscar si ese usuario existe
            //y si la contraseña es igual a la insertada en el cuadro de texto
            //Supon que el txtnombre.text es Administrador , txtpassword 123456 

            //este es el singlento, deberias hacerlo en el load del todo el programa para que este disponible en cualquier formulario
            UsuarioConectado usuarioconectado = UsuarioConectado.Instance;

            var usuariologueado = _usuarioRepositorio.ObTenerPorNombre("Administrador");//Aca iria txtnombre.text
            if (string.IsNullOrEmpty(usuariologueado.NombreUsuario))
            {
                if (usuariologueado.Password== "123456")//Aca iria txtpassword.text
                {
                    //Se guarda la información del usuario logueado en el singlenton para que lo utilices donde quieras
                    usuarioconectado.SalvarDatosDelUsuarioLogueadoPermantemente(usuariologueado);
                    //... abrir tal formulario...
                }
            }


            //Cuando quieras tener los datos del usuario conectado solo haces
            var usuarioActualConectado = usuarioconectado.ObtenerDatosDelUsuarioLogueado();

            if (_usuarioRepositorio.EstaEnRol(usuarioconectado.ObtenerDatosDelUsuarioLogueado().NombreUsuario, "Administrador"))
            {
                //Ejemplotxt.enable = true;
            }
            else
            {
                //Ejemplotxt.enable = false;
            }

            //y si se desloguearon utilizas el metodo desloguear que solo resetea los valores del usuario logueado, solo hace _usuario = new Usuario(); osea crea una instancia pero no tiene valores sino hasta que 
            //se los asignes de nuevo cuando una nueva persona se loguee.

            usuarioconectado.Desloguear();





            #endregion



        }
    }
   
}
