﻿using Login.Infrastructura.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login.Infrastructura.Singlenton
{
    public sealed class UsuarioConectado
    {
   
        static readonly UsuarioConectado _instance = new UsuarioConectado();
        private Usuario _usuario;

        public void SalvarDatosDelUsuarioLogueadoPermantemente(Usuario usuariologueado)
        {
            _usuario =  usuariologueado;
        }

        public Usuario ObtenerDatosDelUsuarioLogueado()
        {
           return _usuario;
        }

        public void Desloguear()
        {
            _usuario = new Usuario() ;
        }

        public static UsuarioConectado Instance
        {
            get
            {
                return _instance;
            }
        }

        UsuarioConectado()
        {           
            _usuario = new Usuario();
        }
    }
}
