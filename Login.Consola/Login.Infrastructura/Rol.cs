﻿using Login.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login.Infrastructura
{
    public class Rol: IRol
    {
        public int Id { get; set; }
        public string Nombre { get; set; }

        //Propiedades De Navegación
        public int IdUsuario { get; set; }
        public Usuario Usuario { get; set; }
    }
}
