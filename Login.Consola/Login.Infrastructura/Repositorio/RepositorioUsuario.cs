﻿using Login.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Login.Infrastructura.Contexto;


namespace Login.Infrastructura.Repositorio
{
    public class RepositorioUsuario : IRepositorio<Usuario>
    {
        private Login.Infrastructura.Contexto.Contexto contexto;
        public RepositorioUsuario()
        {
            contexto = new Contexto.Contexto();
        }


        public void Eliminar(Usuario entidad)
        {
            try
            {
                contexto.Usuario.Remove(entidad);
                contexto.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public void Guardar(Usuario entidad)
        {
            try
            {
                contexto.Usuario.Add(entidad);
                contexto.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public Usuario ObTenerPorId(int id)
        {
            return contexto.Usuario.Include(a => a.Roles).Where(c => c.Id == id).FirstOrDefault();
        }

        public Usuario ObTenerPorNombre(string nombre)
        {
            return contexto.Usuario.Include(a => a.Roles).Where(c => c.NombreUsuario == nombre).FirstOrDefault();

        }

        public List<Usuario> ObtenerTodos()
        {
            return contexto.Usuario.Include(a => a.Roles).ToList();
        }

        public void AgregarRolesAUsuario(int IdUsuario, List<Rol> Roles)
        {
            var usuario = this.ObTenerPorId(IdUsuario);

            usuario.Roles.AddRange(Roles);
            contexto.SaveChanges();
        }

        public bool EstaEnRol(string nombreusuario,string rol)
        {
            var usuario = this.ObTenerPorNombre(nombreusuario);
            var estaenrol = usuario.Roles.Where(c => c.Nombre == rol);
            if (estaenrol != null)
                return true;
            else
                return false;
        }
    }
}
