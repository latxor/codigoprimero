﻿using Login.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;


namespace Login.Infrastructura.Repositorio
{
    public class RepositorioRol : IRepositorio<Rol>
    {

        private Login.Infrastructura.Contexto.Contexto contexto;
        public void Eliminar(Rol entidad)
        {
            try
            {
                contexto.Rol.Remove(entidad);
                contexto.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public void Guardar(Rol entidad)
        {
            try
            {
                contexto.Rol.Add(entidad);
                contexto.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public Rol ObTenerPorId(int id)
        {
            return contexto.Rol.Where(c => c.Id == id).FirstOrDefault();
        }

        public Rol ObTenerPorNombre(string nombre)
        {
            return contexto.Rol.Where(c => c.Nombre == nombre).FirstOrDefault();

        }

        public List<Rol> ObtenerTodos()
        {
            return contexto.Rol.ToList();
        }
    }
}
