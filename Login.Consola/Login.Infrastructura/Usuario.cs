﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Login.Datos;

namespace Login.Infrastructura
{
    public class Usuario: IUsuario
    {
      public int Id { get; set; }
      public string NombreUsuario { get; set; }
      public string Password { get; set; }
      

      //Propiedades De Navegación
      public List<Rol> Roles { get; set; }
    }
}
