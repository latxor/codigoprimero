﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login.Infrastructura.Contexto
{
    public class Contexto: DbContext
    {
        public Contexto():base(@"Server=.\DESARROLLO;Database=Login;User Id=sa;Password=sapassword;")
        {

        }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Rol> Rol { get; set; }
    }
}
