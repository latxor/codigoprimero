﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodigoConsecutivo
{
    class Program
    {
        static void Main(string[] args)
        {

            string codigo = "SBA-0001";
            Console.WriteLine("Codigo: {0}",codigo);
            //Sí el principio del codigo no va a cambiar osea si por ejemplo siempre seria SBA-0 o C001, acá es donde juegas con las substring
            //acá le digo de la cadena almacenada en codigo traeme lo que este despues del 4 caracter en adelante es decir
            //Vuelvate en mi caso SBA- y traeme lo demas, tu caso seria Substring(1) ya que solo es la C
            string subcadena = codigo.Substring(4);
            Console.WriteLine("SubCadena: {0}",subcadena);

            //Como se que la subcadena va a contener un numero lo convierto
            int numero = Convert.ToInt32(subcadena);
            Console.WriteLine("Numero: {0}",numero.ToString());

            //Le sumo uno
            numero++;
            Console.WriteLine("Nuevo Numero: {0}", numero.ToString());

            //y acá le digo del codigo original sacame una subcadena de los primeros 4 caracteres osea SBA- y a ella concatenala con el numero en formato de cadena con longitud minima de 4 caracteres(D4) 
            //D4 = Dependiendo del numero meteles ceros a la izquierda pero el total de la cadena tiene que ser 4 caracteres, por ejemplo si numero es 2(un solo caracter)
            //llenalo con los ceros a la izquierdas hasta que sean 4 carateres es decir 0002, si fuese 100 (son 3 caracteres) solo le meteria 1 solo cero a la izq 0100
            //si es 1000(no le mete por que son 4 caracteres, acá si ves que vas a tener muchos codigo generados lo mejor es que le pongas un D8 o D10)
            var nuevocodigo = codigo.Substring(0,4) + numero.ToString("D4");
            Console.WriteLine("NUEVO CODIGO: {0}", nuevocodigo);
            Console.ReadKey();
            
        }
    }
}
