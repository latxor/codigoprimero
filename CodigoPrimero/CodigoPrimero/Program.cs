﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CodigoPrimero
{
    class Program
    {
        static void Main(string[] args)
        {

            
            #region INICIALIZACION DE DATOS DE LOS PADRES

            Console.WriteLine("Inicializando los datos de padres");


            Padre Padre1 = new Padre() { Nombre = "Juan Perez" };
            Padre Padre2 = new Padre() { Nombre = "Francisco Martinez" };
            

            using (var contexto = new Contexto())
            {
                contexto.Padre.Add(Padre1);
                contexto.Padre.Add(Padre2);
                
                contexto.SaveChanges();

                Console.WriteLine("Padres guardados en la base de datos");

            }

            #endregion

            #region INICIALIZACION DE DATOS DE LOS HIJOS
            Console.WriteLine("Inicializando los datos de padres e hijos");


            List<Hijos> HijosPadre1 = new List<Hijos>();

            HijosPadre1.Add(new Hijos() { Nombre = "Hijo #1 Padre1", Edad = 5 });
            HijosPadre1.Add(new Hijos() { Nombre = "Hijo #2 Padre1", Edad = 18 });

            List<Hijos> HijosPadre2 = new List<Hijos>();

            HijosPadre2.Add(new Hijos() { Nombre = "Hijo #1 Padre2", Edad = 25 });
            HijosPadre2.Add(new Hijos() { Nombre = "Hijo #2 Padre2", Edad = 11 });

            using (var contexto = new Contexto())
            {
                //Busco al padre1 por el nombre
                Padre padre1 = contexto.Padre.Where(c => c.Nombre == "Juan Perez").FirstOrDefault();
                foreach (var hijo in HijosPadre1)
                {
                    //Uno al hijo con su padre a traves del Id
                    hijo.PadreId = padre1.Id;
                    contexto.Hijos.Add(hijo);
                    contexto.SaveChanges();
                }

                Padre padre2 = contexto.Padre.Where(c => c.Nombre == "Francisco Martinez").FirstOrDefault();
                foreach (var hijo in HijosPadre2)
                {
                    hijo.PadreId = padre2.Id;
                    contexto.Hijos.Add(hijo);
                    contexto.SaveChanges();
                }

                Console.WriteLine("Hijos guardados en la base de datos");

            }

            #endregion

            #region IMPRIMIENDO INFORMACION DE PADRES E HIJOS
            Console.WriteLine("IMPRIMIENDO INFORMACIÓN DE LA BASE DE DATOS");

            using (var contexto = new Contexto())
            {
                //Me traigo todos los padres que estan en la base de datos
                var ListaDePadres = contexto.Padre.Include(c=>c.Hijos).ToList();
                foreach (var padre in ListaDePadres)
                {
                    Console.WriteLine("El padre: {0} tiene {1} Hijo(s)", padre.Nombre, padre.Hijos.Count().ToString());
                    Console.WriteLine("El padre: {0} tiene {1} Hijo(s) mayor(es) de edad", padre.Nombre, padre.Hijos.Where(c => c.Edad > 17).Count().ToString());
                    Console.WriteLine("El padre: {0} tiene {1} Hijo(s) menor(es) de edad", padre.Nombre, padre.Hijos.Where(c => c.Edad < 18).Count().ToString());
                    Console.WriteLine("Hijos del padre {0}: ", padre.Nombre);

                    foreach (var hijo in padre.Hijos)
                    {
                         Console.WriteLine("   Nombre: {0} - Edad: {1} ", hijo.Nombre, hijo.Edad);                        
                    
                    }
                    Console.WriteLine("Presione cualquier tecla para seguir");
                    Console.ReadLine();           
                }


            }
            #endregion



        }
    }

    public class Contexto: DbContext
	{
        //Aca tiene que meter tu cadena de conexion, esta es la mia 
        public Contexto()  : base(@"Server=.\DESARROLLO;Database=padresehijos;User Id=sa;Password=sapassword;")
		{
		}

        
        public DbSet<Padre> Padre { get; set; }
        public DbSet<Hijos> Hijos { get; set; }

	}

    public class Padre
    {

        //siempre poner la propiedad Id, entity entiende que esta va a ser la clave primaria y la pone autoincremento en 1
        public int Id { get; set; }
        public string Nombre { get; set; }


        //A ESTO SE LE LLAMA PROPIEDAD DE NAVEGACION, ES LA PROPIEDAD QUE TE PERMITE UNIR ESTA CLASE CON LAS DEMAS CLASES
        //ESTO NO ES MAS QUE UNA REFERENCIA A LA CLASE QUE QUIERES UNIR, ACA DIGO EL PADRE TIENE UNA LISTA DE HIJOS
        //MIRA QUE TIENE LA PALABRA VIRTUAL, CON ESTO ENTITY FRAMEWORK ENTIENDE Y HACE LA UNION DE LAS 2 CLASES COMO UNA RELACION EN BASE DE DATOS 1 A MUCHOS
        //UN PADRE TIENE MUCHOS HIJOS
        public virtual List<Hijos> Hijos { get; set; }
    }

    public class Hijos
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Edad { get; set; }


        //PROPIEDAD DE NAVEGACION  DONDE DICE QUE UN HIJO TIENE UN SOLO PADRE, RELACION 1 A 1 COMO EN LAS BASES DE DATOS
        //Id + La nombre de la clase hace la clave foranea, en este caso es IdPadre esto son puras reglas del entity framework y code firts que debes aprender.
        public virtual int  PadreId { get; set; }
        public virtual Padre  Padre { get; set; }

    }
}
